"""
__author__ = 'loong'
"""
from baye.util import ET, dec_raise_xml


class Skill:
    class meta:
        name = '名称'
        tag = '技能'

    def __init__(self, index):
        self.index = index
        self.name = ''

    def xml(self):
        xml = ET.Element(self.meta.tag)
        xml.attrib[self.meta.name] = self.name
        return xml

    @dec_raise_xml
    def load_xml(self, xml):
        self.name = xml.attrib[self.meta.name]
        return self
