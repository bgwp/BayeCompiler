# coding: utf8
"""
__author__ = 'loong'
"""
import baye.structures as struct
import baye.models as models
from baye import resid
from baye.util import ET, dec_raise_xml
from baye.person import Person
from baye.structures import read_int


class City:
    class meta:
        tag = '城池'
        tag_person = '人物'
        tag_tool = '道具'
        name = '名称'
        belong = '归属'
        satrap = '太守'
        farming = '农业'
        farming_limit = '农业上限'
        commerce = '商业'
        commerce_limit = '商业上限'
        devotion = '民忠'
        avoid = '防灾'
        population = '人口'
        population_limit = '人口上限'
        money = '金钱'
        food = '粮食'
        arms = '后备兵力'
        people = '城中人物'
        tools = '城中道具'
        neighbors = '毗邻城池'

    def __init__(self, period, index):
        self.period = period
        self.index = index
        self.name = ''  # 名称
        self.belong = ''  # 归属
        self.satrap = ''  # 太守
        self.farming = 0  # 农业
        self.farming_limit = 0  # 农业上限
        self.commerce = 0  # 商业
        self.commerce_limit = 0  # 商业上限
        self.devotion = 0  # 民忠
        self.avoid = 0  # 防灾
        self.population = 0  # 人口
        self.population_limit = 0  # 人口上限
        self.money = 0  # 金钱
        self.food = 0  # 粮食
        self.arms = 0  # 后备兵力
        self.people = []
        self.tools = []
        self.neighbors = []

    def xml(self):
        city_node = ET.Element(self.meta.tag)
        attrs = {}
        attrs[self.meta.name] = self.name
        attrs[self.meta.belong] = self.belong
        attrs[self.meta.satrap] = self.satrap
        attrs[self.meta.farming] = self.farming
        attrs[self.meta.farming_limit] = self.farming_limit
        attrs[self.meta.commerce] = self.commerce
        attrs[self.meta.commerce_limit] = self.commerce_limit
        attrs[self.meta.devotion] = self.devotion
        attrs[self.meta.avoid] = self.avoid
        attrs[self.meta.population] = self.population
        attrs[self.meta.population_limit] = self.population_limit
        attrs[self.meta.money] = self.money
        attrs[self.meta.food] = self.food
        attrs[self.meta.arms] = self.arms
        city_node.attrib = {k: str(v) for k, v in attrs.items()}

        people_node = ET.Element(self.meta.people)
        city_node.append(people_node)
        for person in self.people:
            people_node.append(person.xml())

        tools_node = ET.Element(self.meta.tools)
        city_node.append(tools_node)
        for tool in self.tools:
            tool_node = ET.Element(self.meta.tag_tool)
            tools_node.append(tool_node)
            tool_node.attrib[tool.meta.name] = tool.name

        return city_node

    @dec_raise_xml
    def load_xml(self, xml, lib):
        self.name = xml.attrib[self.meta.name]
        self.belong = xml.attrib[self.meta.belong]
        self.satrap = xml.attrib[self.meta.satrap]
        self.farming = int(xml.attrib[self.meta.farming])
        self.farming_limit = int(xml.attrib[self.meta.farming_limit])
        self.commerce = int(xml.attrib[self.meta.commerce])
        self.commerce_limit = int(xml.attrib[self.meta.commerce_limit])
        self.devotion = int(xml.attrib[self.meta.devotion])
        self.avoid = int(xml.attrib[self.meta.avoid])
        self.population = int(xml.attrib[self.meta.population])
        self.population_limit = int(xml.attrib[self.meta.population_limit])
        self.money = int(xml.attrib[self.meta.money])
        self.food = int(xml.attrib[self.meta.food])
        self.arms = int(xml.attrib[self.meta.arms])
        for ch in xml:
            if ch.tag == self.meta.people:
                for pnode in ch:
                    p = Person(self.period, 0)
                    p.load_xml(pnode)
                    p.condition.birth = 0  # people already in city can not birth twice
                    self.people.append(p)
            elif ch.tag == self.meta.tools:
                for tnode in ch:
                    name = tnode.attrib['名称']
                    hide = tnode.attrib.get('隐藏', '1')
                    t = lib.get_tool_by_name(name)
                    t.hide = int(hide)
                    self.tools.append(t)

    def load_bin(self, data):
        info = struct.read_struct(data, 0, models.CITY)
        self.__dict__.update(info)
        return self

    def bin(self):
        data = {'id': self.index + 1}
        data.update(self.__dict__)
        return struct.write_struct(data, models.CITY)

    def link(self):
        period = self.period
        self.name = period.lib.cities[self.index].name
        self.belong = period.get_person_name(self.belong - 1)
        self.satrap = period.get_person_name(self.satrap - 1)

        if period.lib.lib_version == 0:
            gPersonQueue = period.lib.resources[resid.GENERAL_QUEUE].items[period.index]
            gToolQueue = period.lib.resources[resid.GOODS_QUEUE].items[period.index]
        else:
            data = period.lib.resources[resid.GENERAL_QUEUE].items[period.index]
            gPersonQueue = []
            for i in range(int(len(data)/2)):
                ind = read_int(data[i*2:i*2+2], 2)
                gPersonQueue.append(ind)

            data = period.lib.resources[resid.GOODS_QUEUE].items[period.index]
            gToolQueue = []
            for i in range(int(len(data)/2)):
                ind = read_int(data[i*2:i*2+2], 2)
                gToolQueue.append(ind)

        q = gPersonQueue[self.persons_queue: self.persons_queue+self.persons_count]

        for idx in q:
            self.people.append(period.people[idx])

        q = gToolQueue[self.tools_queue: self.tools_queue+self.tools_count]
        for idx in q:
            self.tools.append(period.lib.tools[idx])

    def unlink(self, period):
        self.index = period.lib.get_city_index_by_name(self.name)

        def get_person(name):
            return period.get_person_by_name(name) + 1

        self.belong = get_person(self.belong)
        self.satrap = get_person(self.satrap)

