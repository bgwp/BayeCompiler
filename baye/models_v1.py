"""
__author__ = 'loong'
"""

CITY = [
    (1, 'id'),
    (1, 'belong'),
    (1, 'satrap'),
    (2, 'farming_limit'),
    (2, 'farming'),
    (2, 'commerce_limit'),
    (2, 'commerce'),
    (1, 'devotion'),
    (1, 'avoid'),
    (4, 'population_limit'),
    (4, 'population'),
    (2, 'money'),
    (2, 'food'),
    (2, 'arms'),
    (1, 'persons_queue'),
    (1, 'persons_count'),
    (1, 'tools_queue'),
    (1, 'tools_count'),
]


PERSON = [
    (1, 'id'),
    (1, 'belong'),
    (1, 'level'),
    (1, 'force'),
    (1, 'iq'),
    (1, 'devotion'),
    (1, 'character'),
    (1, 'exp'),
    (1, 'thew'),
    (1, 'arm_type'),
    (2, 'arms'),
    (1, 'equip0'),
    (1, 'equip1'),
    (1, 'age'),
]

TOOL = [
    (1, 'id'),
    (1, 'useflag'),
    (30, 'attack_range', bytes),
    (1, 'change_attack_range'),
    (60-31, 'reserved', bytes),
    (1, 'at'),
    (1, 'iq'),
    (1, 'move'),
    (1, 'arm'),
]

ADDRESS = [
    (4, 'address')
]

CONDITION = [
    (1, 'birth'),
    (1, 'bole'),
    (1, 'city'),
]

RESHEAD = [
    (4, 'resLen'),
    (2, 'resID'),
    (2, 'itemCnt'),
    (2, 'itemLen'),
    (1, 'resKey'),
    (1, 'reserved'),
]

RIDX = [
    (2, 'offset'),
    (2, 'rlen'),
]

PICHEAD = [
    (2, 'width'),
    (2, 'height'),
    (1, 'count'),
    (1, 'mask'),
]

SPERES = [
    (1, 'type'),
    (1, 'idx'),
    (1, 'count'),
    (1, 'picmax'),
    (1, 'startfrm'),
    (1, 'endfrm'),
]

SPEUNIT = [
    (1, 'x'),
    (1, 'y'),
    (1, 'cdelay'),
    (1, 'ndelay'),
    (1, 'picIdx'),
]
