"""
__author__ = 'loong'
"""
from baye import structures
from baye import models


class Condition:
    empty = b'\x00' * 3

    def __init__(self):
        self.birth = 0
        self.bole = 0
        self.city = 0

    def load_bin(self, data):
        info = structures.read_struct(data, 0, models.CONDITION)
        self.__dict__.update(info)

    def bin(self):
        return structures.write_struct(self.__dict__, models.CONDITION)

    def __str__(self):
        return f"Condition<birth={self.birth},bole={self.bole},city={self.city}>"

