"""
__author__ = 'loong'
"""
import sys
import os
parent_path = os.path.join(os.path.dirname(__file__), '..')
sys.path.insert(0, os.path.abspath(parent_path))

from baye.image import Animation, ImageSet

if 'win32' in sys.platform.lower():
    HOME = r"Z:\\"
else:
    HOME = os.path.expanduser('~')


def home(path):
    return os.path.join(HOME, path)

from baye.main import _main as main


def decode_hex(hx):
    import codecs
    hx = ''.join(hx.split())
    return codecs.decode(hx, 'hex')


if __name__ == '__main__':
    # from baye import resid
    # decode_images()
    # main('../dist/qmlw.lib')
    # main('../dist/qmlw.xml')
    main('libs/dat.lib')
    main('libs/dat.res')
    main('libs/dat-mod.lib')
    main('libs/dat-mod.res')
    # main('dat-mod-mod.lib')
    # main('dat-mod-mod.res')
    # main('dat-mod-mod-mod.lib')
    # main('ysh-hard1.6.lib')
    # main(os.path.expanduser('~/tmp/qmlw.xml'))
    # main('dat-mod.lib')
    # main('../dist/dat-ex-mod.lib')
    # main('../../sav/dat.lib')

