"""
__author__ = 'loong'
"""
from lzma import LZMACompressor, FORMAT_ALONE
import base64

tmpl = '''
(function(){
    var b64lib = '%(lib)s';
    loadB64Lib(b64lib, '%(name)s');
})();
'''


def convert2js(data, name):
    print(len(data))
    import codecs
    print(codecs.encode(data, 'hex'))
    lzma = LZMACompressor(format=FORMAT_ALONE)
    compressed = lzma.compress(data) + lzma.flush()
    b64lib = base64.b64encode(compressed).decode('utf8')
    js = tmpl % {'lib': b64lib, 'name': name}
    print(js)
    return js


js = convert2js(open('baye-plus.lib', 'rb').read(), 'plus')

open('三国lib.js', 'wb').write(js.encode('utf8'))

