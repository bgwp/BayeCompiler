"""
__author__ = 'loong'
"""
import os
from baye import image
from baye import models
from baye import structures
from baye.util import ET, guard_parents_path, dec_raise_xml


class Tool:
    class meta:
        tag = '道具'
        name = '名称'
        useflag = '类型'
        description = '描述'
        at = '加武力'
        iq = '加智力'
        move = '加速度'
        arm = '变兵种'

    def __init__(self, index):
        self.index = index
        self.useflag = 0
        self.name = ''
        self.description = ''
        self.at = 0
        self.iq = 0
        self.move = 0
        self.arm = 0
        self.change_attack_range = 0
        self.attack_range = b''

    @classmethod
    def parse_bin(cls, index, data):
        tool = Tool(index)
        info = structures.read_struct(data, 0, models.TOOL)
        tool.__dict__.update(info)
        tool.index = index
        return tool

    def bin(self):
        ctx = {'id': self.index + 1, 'reserved': b'\x00' * (60-31)}
        ctx.update(self.__dict__)
        ctx.update(name='', description='')
        return structures.write_struct(ctx, models.TOOL)

    def xml(self):
        i = ET.Element(self.meta.tag)
        i.attrib[self.meta.name] = self.name
        i.attrib[self.meta.description] = self.description
        i.attrib[self.meta.useflag] = self.useflag and '使用' or '装备'
        i.attrib[self.meta.at] = str(self.at)
        i.attrib[self.meta.iq] = str(self.iq)
        i.attrib[self.meta.move] = str(self.move)
        i.attrib[self.meta.arm] = {
            1: '水兵',
            2: '玄兵',
            3: '极兵',
            4: '骑兵',
            5: '步兵',
            6: '弓兵',
        }.get(self.arm, self.arm and str(self.arm) or '')
        return i

    @classmethod
    def from_xml(cls, index, xml):
        tool = Tool(index)
        tool.load_xml(xml)
        return tool

    @dec_raise_xml
    def load_xml(self, xml):
        self.name = xml.attrib[self.meta.name]
        self.description = xml.attrib[self.meta.description]
        self.useflag = {"使用": 1, "装备": 0}.get(xml.attrib[self.meta.useflag], 0)
        self.at = int(xml.attrib[self.meta.at])
        self.iq = int(xml.attrib[self.meta.iq])
        self.move = int(xml.attrib[self.meta.move])
        armtype = xml.attrib[self.meta.arm]
        try:
            mp = {'水兵': 1,
                  '玄兵': 2,
                  '极兵': 3,
                  '骑兵': 4,
                  '步兵': 5,
                  '弓兵': 6,
                  '': 0
                  }
            self.arm = mp[armtype]
        except KeyError:
            if armtype.isdigit() and int(armtype) < 256:
                self.arm = int(armtype)
            else:
                raise Exception('道具{}的兵种错误'.format(self.name))

    def dump_att_range(self, path):
        fullpath = os.path.abspath(os.path.join(path, self.name + '.bmp'))
        guard_parents_path(fullpath)
        if self.change_attack_range:
            img = image.Image(15, 15, self.attack_range)
            fullpath = os.path.abspath(os.path.join(path, self.name + '.bmp'))
            guard_parents_path(fullpath)
            img.save(fullpath)

    def load_att_range(self, path):
        try:
            img = image.Image.open(os.path.join(path, self.name + '.bmp'), 15, 15)
            self.change_attack_range = 1
            self.attack_range = img.data
        except OSError:
            self.change_attack_range = 0
            self.attack_range = b'\x00' * 30
