# coding: utf8

"""
__author__ = 'loong'
"""

IFACE_STRID = 1  # /* 引擎界面字符串资源id */
IFACE_CONID = 2  # /* 引擎界面常量资源id */
MAIN_SPE = 3  # /* 游戏开头动画 */
FIGHT_TILE = 4  # /* 战斗地图tile */
BING_PIC = 5  # /* 兵种图片 */
MAKER_SPE = 6  # /* 开发组群 */
STEP_PIC = 7  # /* 脚步图片 */
STATE_PIC = 8  # /* 状态显示图片 */
WEATHER_PIC = 9  # /* 天气图片 */
SKL_RESID = 10  # /* 技能资源 */
SKL_NAMID = 11  # /* 技能名字 */
SKL_EXPID = 12  # /* 技能说明 */
SKL_RNGID = 13  # /* 技能施展范围 */
SPE_SKLID = 14  # /* 将领专长技能 */
NUM_PICID = 15  # /* 数字图片ID */
SPE_BACKPIC = 16  # /* 特效背景图片 */

QIBING_SPE = 19  # /* 骑兵攻击特效 */
BUBING_SPE = 20  # /* 步兵攻击特效 */
JIANBING_SPE = 21  # /* 弓兵攻击特效 */
SHUIJUN_SPE = 22  # /* 水军攻击特效 */
JIBING_SPE = 23  # /* 极兵攻击特效 */
XUANBING_SPE = 24  # /* 玄兵攻击特效 */
SHUISHANG_SPE = 25  # /* 水面攻击特效 */
MSGBOX_PIC = 26  # /* 消息框背景图片 */
STACHG_SPE = 27  # /* 升级了/死亡了-特效 */
WEATHER_PIC1 = 28  # /* 天气图片-晴 */
WEATHER_PIC2 = 29  # /* 天气图片-阴 */
WEATHER_PIC3 = 30  # /* 天气图片-风 */
WEATHER_PIC4 = 31  # /* 天气图片-雨 */
WEATHER_PIC5 = 32  # /* 天气图片-冰 */
DAYS_PIC = 33  # /* 天数-背景图片 */
SPESTA_PIC = 34  # /* 特殊状态-禁咒、定身图片 */
FIRE_SPE = 35  # /* 火攻烈火火箭 */
WATER_SPE = 36  # /* 水淹海啸 */
WOOD_SPE = 37  # /* 滚木|礌石 */
BUMP_SPE = 38  # /* 撞击 */
FENG_SPE = 39  # /* 封|定 */
LIUYAN_SPE = 40  # /* 流言 */
YUAN_SPE = 41  # /* 援兵|援军 */
ZHEN_SPE = 42  # /* 石阵 */
XJING_SPE = 43  # /* 陷阱 */
MAIN_PIC = 44  # /* 主菜单背景图片 */
YEAR_PIC = 45  # /* 历史时期背景图片 */
SAVE_PIC = 46  # /* 存档管理界面背景图 */
CITY_PIC = 47  # /* 城市缩略图 */
GEN_HEADPIC1 = 48  # /* 将领头像图片-董卓弄权 */
GEN_HEADPIC2 = 49  # /* 将领头像图片-曹操崛起 */
GEN_HEADPIC3 = 50  # /* 将领头像图片-赤壁之战 */
GEN_HEADPIC4 = 51  # /* 将领头像图片-三足鼎立 */

CITYMAP_TILE = 54  # /* 城市地图tile */
CITY_ICON = 55  # /* 城市图标 */
CITY_POS_ICON = 56  # /* 城市选择光标 */
CITY_RESID = 57  # /* 城市资源 */
CITY_NAME = 58  # /* 城市名称 */
CITY_LINKR = 59  # /* 右向城市连接 */
CITY_LINKL = 60  # /* 左向城市连接 */
GENERAL_RESID = 61  # /* 武将资源 */
GENERAL_NAME = 62  # /* 武将名称一 */
GENERAL_CON = 63  # /* 武将出现条件 */
STRING_CONST = 64  # /* 字符串常量 */
GENERAL_QUEUE = 65  # /* 武将队列 */
GOODS_RESID = 66  # /* 道具资源 */
GOODS_CON = 67  # /* 道具出现条件 */
GOODS_QUEUE = 68  # /* 道具队列 */
MAPFACE_ICON = 69  # /* 显示界面武将及城池图标 */
GENERAL_NAME2 = 70  # /* 武将名称二 */
GENERAL_NAME3 = 71  # /* 武将名称三 */
GENERAL_NAME4 = 72  # /* 武将名称四 */
GOODS_NAME = 73  # /* 道具名称 */
GOODS_INF = 74  # /* 道具说明 */
TACTIC_ICON = 75  # /* 战略图 */
FIGHT_NOTE_ICON = 76  # /* 兵戎提示图 */

ENGINE_SCRIPT = 77  # /* 引擎JS脚本 */

MAIN_ICON1 = 100  # /* 主菜单icon1 */
MAIN_ICON2 = 101  # /* 主菜单icon2 */
MAIN_ICON3 = 102  # /* 主菜单icon3 */
MAIN_ICON4 = 103  # /* 主菜单icon4 */
YEAR_ICON1 = 104  # /* 年限icon1 */
YEAR_ICON2 = 105  # /* 年限icon2 */
YEAR_ICON3 = 106  # /* 年限icon3 */
YEAR_ICON4 = 107  # /* 年限icon4 */

FIGHT_MAP1 = 110  # /* 战斗地图1 */
FIGHT_MAP2 = 111  # /* 战斗地图2 */
FIGHT_MAP3 = 112  # /* 战斗地图3 */
FIGHT_MAP4 = 113  # /* 战斗地图4 */
FIGHT_MAP5 = 114  # /* 战斗地图5 */
FIGHT_MAP6 = 115  # /* 战斗地图6 */
FIGHT_MAP7 = 116  # /* 战斗地图7 */

all_ids = [
    (1, "/* 引擎界面字符串资源id */"),
    (2, "/* 引擎界面常量资源id */"),
    (3, "/* 游戏开头动画 */"),
    (4, "/* 战斗地图tile */"),
    (5, "/* 兵种图片 */"),
    (6, "/* 开发组群 */"),
    (7, "/* 脚步图片 */"),
    (8, "/* 状态显示图片 */"),
    (9, "/* 天气图片 */"),
    (10, "/* 技能资源 */"),
    (11, "/* 技能名字 */"),
    (12, "/* 技能说明 */"),
    (13, "/* 技能施展范围 */"),
    (14, "/* 将领专长技能 */"),
    (15, "/* 数字图片ID */"),
    (16, "/* 特效背景图片 */"),

    (19, "/* 骑兵攻击特效 */"),
    (20, "/* 步兵攻击特效 */"),
    (21, "/* 弓兵攻击特效 */"),
    (22, "/* 水军攻击特效 */"),
    (23, "/* 极兵攻击特效 */"),
    (24, "/* 玄兵攻击特效 */"),
    (25, "/* 水面攻击特效 */"),
    (26, "/* 消息框背景图片 */"),
    (27, "/* 升级了/死亡了-特效 */"),
    (28, "/* 天气图片-晴 */"),
    (29, "/* 天气图片-阴 */"),
    (30, "/* 天气图片-风 */"),
    (31, "/* 天气图片-雨 */"),
    (32, "/* 天气图片-冰 */"),
    (33, "/* 天数-背景图片 */"),
    (34, "/* 特殊状态-禁咒、定身图片 */"),
    (35, "/* 火攻烈火火箭 */"),
    (36, "/* 水淹海啸 */"),
    (37, "/* 滚木|礌石 */"),
    (38, "/* 撞击 */"),
    (39, "/* 封|定 */"),
    (40, "/* 流言 */"),
    (41, "/* 援兵|援军 */"),
    (42, "/* 石阵 */"),
    (43, "/* 陷阱 */"),
    (44, "/* 主菜单背景图片 */"),
    (45, "/* 历史时期背景图片 */"),
    (46, "/* 存档管理界面背景图 */"),
    (47, "/* 城市缩略图 */"),
    (48, "/* 将领头像图片-董卓弄权 */"),
    (49, "/* 将领头像图片-曹操崛起 */"),
    (50, "/* 将领头像图片-赤壁之战 */"),
    (51, "/* 将领头像图片-三足鼎立 */"),

    (54, "/* 城市地图tile */"),
    (55, "/* 城市图标 */"),
    (56, "/* 城市选择光标 */"),
    (57, "/* 城市资源 */"),
    (58, "/* 城市名称 */"),
    (59, "/* 右向城市连接 */"),
    (60, "/* 左向城市连接 */"),
    (61, "/* 武将资源 */"),
    (62, "/* 武将名称一 */"),
    (63, "/* 武将出现条件 */"),
    (64, "/* 字符串常量 */"),
    (65, "/* 武将队列 */"),
    (66, "/* 道具资源 */"),
    (67, "/* 道具出现条件 */"),
    (68, "/* 道具队列 */"),
    (69, "/* 显示界面武将及城池图标 */"),
    (70, "/* 武将名称二 */"),
    (71, "/* 武将名称三 */"),
    (72, "/* 武将名称四 */"),
    (73, "/* 道具名称 */"),
    (74, "/* 道具说明 */"),
    (75, "/* 战略图 */"),
    (76, "/* 兵戎提示图 */"),
    (77, "/* 脚本 */"),

    (100, "/* 主菜单icon1 */"),
    (101, "/* 主菜单icon2 */"),
    (102, "/* 主菜单icon3 */"),
    (103, "/* 主菜单icon4 */"),
    (104, "/* 年限icon1 */"),
    (105, "/* 年限icon2 */"),
    (106, "/* 年限icon3 */"),
    (107, "/* 年限icon4 */"),

    (110, "/* 战斗地图1 */"),
    (111, "/* 战斗地图2 */"),
    (112, "/* 战斗地图3 */"),
    (113, "/* 战斗地图4 */"),
    (114, "/* 战斗地图5 */"),
    (115, "/* 战斗地图6 */"),
    (116, "/* 战斗地图7 */"),
]


def get_comment(resid, idx):
    if resid == IFACE_CONID:
        comments = [
            "/* 兵种攻击范围(按兵种排序) */",
            "/* 兵种策略(按兵种排序) */",
            "/* 各种地形对不同兵种的移动力影响 */",
            "/* 各种地形对不同兵种的战力量影响 草地 平原 山地 森林 村庄 城池 营寨 河流 */",
            "/* 战斗初始化坐标 攻击方*8+防守方 */",
            "/* 城市数组 */",

            "/* 道具属性显示宽度 */",
            "/* 武将属性显示宽度 */",
            "/* 无用数据 */",
            "/* 指令消耗体力		任 开 招 搜 治 出 招 处 流 赏 没 交 宴 输 移 离 招 策 反 劝 无 无 无 侦 征 分 掠 出 */",
            "/* 指令消耗金钱		 任  开  招  搜  治  出  招  处  流  赏  没  交  宴  输  移  离  招  策  反  劝  无  无  无  侦  征  分  掠  出 */",
            "/* 君主产生“内政、协调” 策略几率 */",
            "/* 君主产生“外交”、“军备”策略几率 */",
            "/* city map */",
            "/* 城市战斗地图ID */",
            "/* 引擎参数设置 */",
            "/* 兵种起始移动力 */",
            "/* 相克系数 */",
            "/* 各兵种攻击系数 */",
            "/* 各兵种防御系数 */",
            "/* 各种地形防御系数 */",
            "/* 技能的特效id */",
            "/* 技能播放的起始帧 */",
            "/* 技能播放的终止帧 */",
            "/* 技能播放的坐标 */",
            "/* 技能背景模式, 1为有背景 0为没背景 */"
        ]
        if idx >= len(comments):
            return ''
        else:
            return comments[idx]
    else:
        return ''


class IFACE_ITEM_IDS:
    (dFgtCtMove,
    dFgtNoAim,
    dFgtErrAim,
    dFgtWhrErr,
    dFgtEthErr,
    dFgtMpErr,
    dFgtTrnInf,
    dFgtMnuCmd,
    dFgtSysMnu,
    dFgtLookMnu,
    dFgtMoveSpe,
    dFgtGenTyp,
    dFgtHlpGen,
    dTerrInf0 ,
    dTerrInf1 ,
    dTerrInf2 ,
    dTerrInf3 ,
    dTerrInf4 ,
    dTerrInf5 ,
    dTerrInf6 ,
    dTerrInf7 ,
    dFgtGetExp,
    dFgtArmsH ,
    dFgtArmsA ,
    dFgtProvH ,
    dFgtJNFail,
    dFgtInSta ,
    dFgtState0,
    dFgtState1,
    dFgtState2,
    dFgtState3,
    dFgtState4,
    dFgtState5,
    dFgtState6,
    dFgtState7,
    dFgtState ,
    dFgtNoProv0,
    dFgtNoProv1,
    dFgtLevUp0,
    dFgtLevUp1,
    dFgtLevUp2,
    dFgtDead0 ,
    dFgtDead1 ,
    dFgtDead2 ,
    dChoseKing,
    dGamConErr,
    dGamVarErr,
    dDaysInf ,
    dPowerCmp,
    dArmyInf ,
    dMainGen ,
    dProvInf ,
    dNoView,
    dReserve0,
    dReserve1,
    dReserve2,
    dReserve3,
    dReserve4,
    dReserve5,
    dReserve6,
    dReserve7,
    dReserve8,
    dReserve9,
    dSaveFNam ,
    dRecordInf,
    dNullFNam ,
    dWriting  ,
    dReading  ,
    dErrInf  ,
    dErrInf1
     ) = range(0, 70)

class IFACE_CONST_IDS:
    (dFgtAtRange,
    dFgtJNArray,
    dFgtLandR,
    dFgtLandF,
    dFgtIntPos,
    dCityPos,
    GOODS_PRO_WID,
    PERSON_PRO_WID,
    DirectP,
    ConsumeThew,
    ConsumeMoney,
    KingTacticOddsIH,
    KingTacticOddsD,
    C_MAP,
    dCityMapId,
    dEngineConfig,
    dFgtIntMove,
    dSubduModu,
    dAtkModulus,
    dDfModulus,
    dTerrDfModu,
    kdJNSpeId,
    kdJNSpeSFrm,
    kdJNSpeEFrm,
    kdJNSpeSX,
    kdJNMode) = range(26)
