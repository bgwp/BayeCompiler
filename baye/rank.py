#!/usr/bin/env python
# encoding: utf-8

from baye import parse_sav
import pprint
import json
import time
import pymysql.cursors

connection = pymysql.connect(host='localhost', user='root', password='123456', db='airbox', charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)

with connection.cursor() as cursor:
    # cursor.execute("select * from game_record WHERE game_id = 1 and status = 0 order by user_id,name asc;")
    cursor.execute("select r.id, r.user_id as user_id, r.name as name, r.data as data, u.name as user_name from game_record r join users u on r.user_id = u.id WHERE r.game_id = 1 and (r.status = 0 or r.status is null) order by r.user_id,r.name asc;")
    rank = []
    results = cursor.fetchall()
    records = []
    game_id = 1
    for row in results:
        for i in range(0, len(results), 2):
            print(results[i]['name'], results[i+1]['name'])
            name = results[i]['name']
            user_id = results[i]['user_id']
            user_name = results[i]['user_name']
            sav0 = results[i]['data']
            sav1 = results[i + 1]['data']
            #print(sav0, sav1)
            result = parse_sav.parse_sav(sav0, sav1, 1)
            #pprint.pprint(result)
            king = result['king']

            people_score = 0
            city_score = 0
            city_count = 0
            city_count_total = 0
            people_count = 0
            people_count_total = 0
            arms_count = 0
            arms_count_total = 0
            money_count = 0
            money_count_total = 0
            farming_count = 0
            farming_count_total = 0
            commerce_count = 0
            commerce_count_total = 0
            food_count = 0
            food_count_total = 0
            population_count = 0
            population_count_total = 0

            for item in result['cities']:
                if item['belong']== king + 1:
                    city_score += 500 + 0.2 * item['farming'] + 0.1 * item['commerce'] + 0.3 * item['food'] + 0.2 * item['money']
                    city_count += 1
                    arms_count += item['arms']
                    money_count += item['money']
                    farming_count += item['farming']
                    commerce_count += item['commerce']
                    food_count += item['food']
                    population_count += item['population']
                city_count_total += 1
                arms_count_total += item['arms']
                money_count_total += item['money']
                farming_count_total += item['farming']
                commerce_count_total += item['commerce']
                food_count_total += item['food']
                population_count_total += item['population']

            for item in result['people']:
                if item['belong']== king + 1:
                    people_score += item['force'] * 0.9 * item['level'] + item['iq'] + 0.7 * item['level']
                    people_count += 1
                    arms_count += item['arms']
                people_count_total += 1
                arms_count_total += item['arms']

            print('city_score:' + str(city_score))
            print('people_score:' + str(people_score))
            total = people_score + city_score
            total = int(total * 0.1 )
            print('total', total);
            sql = 'select * from game_statistics where game_id = %s and user_id = %s and name = %s'
            cursor.execute(sql, (game_id, user_id, results[i]['name']))
            gameranks = cursor.fetchall()
            #  print(gameranks)
            #  exit();
            if len(gameranks) > 0:
                cursor.execute("update game_statistics set score = %s, updated = now(), people_count = %s, people_count_total = %s, city_count = %s, city_count_total = %s, arms_count = %s, arms_count_total = %s, money_count = %s, money_count_total = %s, farming_count = %s, farming_count_total = %s, commerce_count = %s, commerce_count_total = %s, food_count = %s, food_count_total = %s, population_count = %s, population_count_total = %s where game_id = %s and user_id = %s and name = %s", (total, people_count, people_count_total, city_count, city_count_total, arms_count, arms_count_total, money_count, money_count_total, farming_count, farming_count_total, commerce_count, commerce_count_total, food_count, food_count_total, population_count, population_count_total, game_id, user_id, name))
            else:
                cursor.execute("insert into game_statistics (game_id, user_id, score, name, updated, created, people_count, people_count_total, city_count, city_count_total, arms_count, arms_count_total, money_count, money_count_total, farming_count, farming_count_total, commerce_count, commerce_count_total, food_count, food_count_total, population_count, population_count_total, user_name) values(%s, %s, %s, %s, now(), now(), %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (game_id, user_id, total, name, people_count, people_count_total, city_count, city_count_total, arms_count, arms_count_total, money_count, money_count_total, farming_count, farming_count_total, commerce_count, commerce_count_total, food_count, food_count_total, population_count, population_count_total, user_name))

            cursor.execute("update game_record set status = 1 where id in(%s,%s)", (results[i]['id'], results[i+1]['id']))
            connection.commit()
    connection.close()

#  if __name__ == '__main__':
#  king = result.king
#  print getPeopleScore(king, result.people)
#  print getPeopleScore
