from setuptools import setup, find_packages
print(find_packages())
setup(
    name = 'BayeCompiler',
    version = '2.0.3',
    packages = find_packages(),
    author = 'Kevin Wang',
    author_email = 'wy721@qq.com',
    url = 'http://gitee.io/bgwp/BayeCompiler',
    description = 'BBK baye lib compiler',
    install_requires=[
        'Pillow',
    ],
)
